/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */

 class Card {
   constructor(suit, val, displayVal) {
     this.suit = suit;
     this.val  = val;
     this.displayVal = displayVal;
   }
};

const getDeck = () => {

  let cardDeck = [];

  const suits = ['Hearts','Diamonds','Clubs','Spades'];
  const cardVals = ['2','3','4','5','6','7','8','9','10','J','Q','K','A'];

  let cardVal;

  for(let suit of suits) {
    for (let card of cardVals) {
      if(parseInt(card)) {
        cardVal = parseInt(card);
      } else {

        cardVal = card == 'A' ? 11 : 10;
      }

      cardDeck.push(new Card(suit,cardVal,card));

    };
  };

  return cardDeck;
};

// CHECKS
const deck = getDeck();
console.log(deck);
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);
