let blackjackDeck = shuffleDeck(getDeck());
// console.log('-----------');
// console.log(blackjackDeck);

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  constructor(name, hand, turn){
    this.name = name;
    this.hand = hand;
    this.turn = turn;
  }

  drawCard() {
    let {name,hand} = this;

    //takes the top card off the deck
    hand = hand.push(blackjackDeck[0]);

    //Removes the card from the deck so it cannot be pulled again
    blackjackDeck.shift();
  };
};

// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer('Dealer', [], false);
const player = new CardPlayer('Player', [], false);

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  let isSoft;
  let allAcesAsOne = 0;
  let cardTotal = 0;

  //Calc hand points
  let hasAces = hand.filter((card) => card.displayVal == 'A');
  isSoft = hasAces.length == 0 ? false : hasAces.length > 1 ? true : false;

  let otherCards = hand.filter((card) => card.displayVal != 'A');
  cardTotal = otherCards.reduce(function(a,b) { return a+b.val }, 0);

  if(isSoft) {
    hasAces.forEach((card) => {
      if(cardTotal >= 10) {
        cardTotal += 1;
        allAcesAsOne += 1;
      } else {
        cardTotal += card.val;
      }
    });

    isSoft = allAcesAsOne == hasAces.length ? false : isSoft;
  } else {
    if(hasAces.length == 1) {
      if(cardTotal > 10) {cardTotal += 1}
      else {
        cardTotal += 11
        isSoft = true;
      }
    }
  }

  let blackJackScore = {
    total: cardTotal,
    isSoft: isSoft
  };

  return blackJackScore;
};


//console.log(`Player points: ${calcPoints(player.hand).total}`);
//console.log(`Dealer points: ${calcPoints(dealer.hand).total}`);

/**
 * Determines whether the dealer should draw another card.
 *
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  let dealerScore = calcPoints(dealerHand);

  if(dealerScore.total <= 16 || (dealerScore.total === 17 && dealerScore.isSoft === true )) { return true; }
  else { return false; }
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore
 * @param {number} dealerScore
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  let winner;

  if(playerScore > 21 ) {winner = `Player busts with ${playerScore}! Dealer WINS with ${dealerScore}`}
  else if(dealerScore > 21 ) {winner = `Dealer busts with ${dealerScore}! Player WINS with ${playerScore}` }
  else if(playerScore == 21) {
      winner = `Player hits BLACKJACK! ${playerScore} - Dealer lost with ${dealerScore}`
      showHand(dealer);
  }
  else if( dealerScore == 21 ) {
    winner = `Dealer hits BLACKJACK! ${dealerScore} - Player lost with ${playerScore}`
    showHand(dealer);
  }
  else if(playerScore == dealerScore ) { winner = `It's a draw! Player ties Dealer at ${playerScore}`}
  else {
    winner = playerScore > dealerScore ? `Player WINS with ${playerScore}` : `Dealer WINS with ${dealerScore}`
  }

  document.getElementById('winner').innerHTML = winner;

  let reloadBTN = document.getElementById('reload');
  reloadBTN.style.display = "block"
  reloadBTN.setAttribute('onclick',"window.location.reload()");
  document.getElementById('userActions').style.display = "none";

  return winner;
};

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count
 * @param {string} dealerCard
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal} (${dealerCard.val}), your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player
 */
const showHand = (player) => {
  // const displayHand = player.hand.map((card) => card.displayVal);
  // console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);

  let pDiv = player.name == 'Player' ? document.getElementById('player') : document.getElementById('dealer');
  let innerHTML;
  let imgElement

  (player.hand).forEach((card, i) => {

    if(!player.turn && i==0) {
        imgElement = `<img src="images/cardBack.png" />`;
    }
    else { imgElement = `<img src="images/${card.suit}/${card.displayVal+card.suit}.png" />`; }

    if(innerHTML == null) {
      innerHTML = imgElement;
    } else {
      innerHTML = innerHTML +'\n'+ imgElement;
    }
  });

  pDiv.innerHTML = innerHTML;

}

/**
 * Runs Blackjack Game
 */
const startGame = function() {

  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  //Hide Start Game button so it cannot trigger a new game in the middle
  document.getElementById('startGame').style.display = "none";
  document.getElementById('userActions').style.display = "block";

  let playerScore = (calcPoints(player.hand)).total;
  console.log(playerScore);
  let dealerScore = (calcPoints(dealer.hand)).total;
  console.log(dealerScore);
  player.turn = true;
  showHand(dealer);
  showHand(player);

  if(playerScore == 21 || dealerScore == 21) {
    player.turn = false;
    dealer.turn = true;
    return determineWinner(playerScore, dealerScore);
  } else {
    document.getElementById('winner').innerHTML = getMessage(playerScore,dealer.hand[1]);
  }
};

const hit = function() {

  let currentPlayer = player.turn ? player : dealer;
  let otherPlayer = player.turn ? dealer : player;
  console.log(currentPlayer);

  currentPlayer.drawCard();
  let currentPoints = calcPoints(currentPlayer.hand).total;
  if(player.turn) {
    showHand(currentPlayer);
    document.getElementById('winner').innerHTML = getMessage(currentPoints,dealer.hand[1]);
  }

  if(currentPoints >= 21) {
    player.turn = false;
    otherPlayer.turn = true;
    showHand(otherPlayer);
    return determineWinner(calcPoints(player.hand).total, calcPoints(dealer.hand).total);
  }

};

//Ends players turn and launches dealer turn loop
const stay = function() {
  document.getElementById('userActions').style.display = "none";

  let currentPlayer = player.turn ? player : dealer;
  let otherPlayer = player.turn ? dealer : player;

  currentPlayer.turn = false;
  otherPlayer.turn = true;

  let playerPoints = calcPoints(currentPlayer.hand).total;

  showHand(otherPlayer);

  let otherPlayerPoints = calcPoints(otherPlayer.hand).total;

  while( otherPlayerPoints < 21 && dealerShouldDraw(otherPlayer.hand) ) {
    otherPlayer.drawCard();
    showHand(otherPlayer);
    otherPlayerPoints = calcPoints(otherPlayer.hand).total;
  }

  return determineWinner(playerPoints, otherPlayerPoints);

}
