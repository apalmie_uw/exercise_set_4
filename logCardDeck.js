const cards = getDeck();

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
};
/**
 * Logs every property of every card to the console
 *
 * @param {array} deck A deck of cards
 */
const logCardDeck = deck => {

  let pDiv = document.getElementById('cardArray');
  let innerHTML;

  deck.forEach((card) => {

    let keys = Object.keys(card);
    for(let key of keys) {
      console.log(`Card ${key} is ${card[key]}`);
    }

    //Displays the dec in the HTML output
    let imgElement = `<img src="images/${card.suit}/${card.displayVal+card.suit}.png" />`;
    if(innerHTML == null) {
      innerHTML = imgElement;
    } else {
      innerHTML = innerHTML +'\n'+ imgElement;
    }



  });

  pDiv.innerHTML = innerHTML;

};

logCardDeck(cards);
